/*
 * This file is part of OGS Engine SDK
 * Copyright (C) 1996-1997 Id Software, Inc.
 * Copyright (C) 2024 BlackPhrase
 *
 * OGS Engine SDK is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OGS Engine SDK is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OGS Engine SDK. If not, see <http://www.gnu.org/licenses/>.
 */

/// @file

#pragma once

//===============
//   TYPES
//===============

/// Wad compression types
enum
{
	CMP_NONE = 0,
	CMP_LZSS
};

/// Wad lump types
enum
{
	TYP_NONE = 0,
	TYP_LABEL,

	TYP_LUMPY = 64, // 64 + grab command number
	TYP_PALETTE = 64,
	TYP_QTEX,
	TYP_QPIC,
	TYP_SOUND,
	TYP_MIPTEX = 68,
	// TODO: something type 69,
	TYP_QFONT = 70
};

// TODO: qpic_t?

typedef struct
{
	char identification[4]; // should be WAD2/WAD3 or 2DAW/3DAW
	int numlumps;
	int infotableofs;
} wadinfo_t;

#ifndef TEXTURE_NAME_LENGTH
#	define TEXTURE_NAME_LENGTH 16
#endif

typedef struct
{
	int filepos;
	int disksize;
	int size; // uncompressed
	char type;
	char compression;
	char pad1, pad2;
	char name[TEXTURE_NAME_LENGTH]; // must be null terminated
} lumpinfo_t;

// TODO: texlumpinfo_t?

// TODO: miptex_t?